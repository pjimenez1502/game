package Game.Terrain;

import Game.GameEntity;
import Game.Map.GameMap;
import javafx.scene.canvas.GraphicsContext;

public abstract class Terrain extends GameEntity {

    public Terrain(int x, int y, String imgstr, GameMap map, GraphicsContext gc){
        super(x,y,imgstr,map,gc);

        tile.setCont(this);

        draw();
    }

    @Override
    public void draw() {
        super.draw();
    }

    @Override
    protected void death() {

    }
}
