package Game.Terrain;

import Game.Map.GameMap;
import Game.Player.Player;
import Game.Structures.MoonBase;
import javafx.scene.canvas.GraphicsContext;

public class Moon extends Terrain{

    MoonBase moonBase;

    public Moon(int x, int y, GameMap map, GraphicsContext gc) {

        super(x, y, "moon.png", map, gc);
    }

    public void buildBase(Player player){
        this.moonBase = new MoonBase(coords.x, coords.y, map, gc, player);
    }

    public MoonBase getMoonBase() {
        return moonBase;
    }
}
