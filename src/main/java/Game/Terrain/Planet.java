package Game.Terrain;

import Game.Map.GameMap;
import Game.Player.Player;
import Game.Structures.PlanetBase;
import javafx.scene.canvas.GraphicsContext;

public class Planet extends Terrain{

    PlanetBase planetBase;

    public Planet(int x, int y, GameMap map, GraphicsContext gc) {

        super(x, y, "planet.png", map, gc);
    }

    public void buildBase(Player player){
        this.planetBase = new PlanetBase(coords.x, coords.y, map, gc, player);
    }

    public PlanetBase getPlanetBase() {
        return planetBase;
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
