package Game.Units;

import Game.Map.GameMap;
import Game.Player.Player;
import javafx.scene.canvas.GraphicsContext;

public class AssaultShip extends Unit {


    public AssaultShip(int x, int y, GameMap map, GraphicsContext gc, Player player) {
        super(x, y, "AssaultShip.png", map, gc, player);

        maxHealth = 5000;
        health = maxHealth;
        weapon = 12;
        speed = 0.5;

    }

    public static int[] getPrice(){
        return new int[]{450, 200};
    }

}
