package Game.Units;

import Game.Map.GameMap;
import Game.Player.Player;
import javafx.scene.canvas.GraphicsContext;

public class Destroyer extends Unit {


    public Destroyer(int x, int y, GameMap map, GraphicsContext gc, Player player) {
        super(x, y, "Destroyer.png", map, gc, player);

        maxHealth = 15000;
        health = maxHealth;
        weapon = 15;
        speed = 0.2;

    }

    public static int[] getPrice(){
        return new int[]{1000, 500};
    }

}
