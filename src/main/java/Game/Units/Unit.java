package Game.Units;

import Game.Coords;
import Game.GameEntity;
import Game.Map.GameMap;
import Game.Map.Tile;
import Game.Player.Player;
import Game.Structures.Structure;
import Game.Terrain.Moon;
import Game.Terrain.Planet;
import Game.Terrain.Terrain;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.util.ArrayList;


public abstract class Unit extends GameEntity {


    enum Action {
        IDLE, MOVE, ATTACK, BUILD
    }

    protected Action unitAction;

    protected Player player;

    private Tile objective;

    protected double speed;

    private Image objectiveImg;

    protected boolean available = true;

    ArrayList<Tile> objectiveList = new ArrayList<>();

    float weapon;

    public Unit(int x, int y, String imgstr, GameMap map, GraphicsContext gc, Player player){

        super(x,y,imgstr,map,gc);

        this.objective = this.tile;
        objectiveImg = new Image("objective.png");

        tile.setCont(this);

        this.player = player;

        unitAction = Action.IDLE;

    }

    @Override
    public void update() {

        switch (unitAction){
            case IDLE:
                break;

            case MOVE:
                move(objective);
                break;

            case ATTACK:
                attack(objective);
                break;
            case BUILD:
                build(objective);
                break;
        }

        if (available && !objectiveList.isEmpty()){
            setObjfromList();
        }

    }

    @Override
    public void draw() {
        super.draw();

        gc.setStroke(player.getColor());
        gc.strokeText(health + "/" + maxHealth, coords.rawX+20, coords.rawY+80);
    }

    protected void build(Tile objective) {
    }

    public void setObjective(Tile tile) {

//        System.out.println("Objective: "+ tile.getCoords().x + "-" + tile.getCoords().y);
//        System.out.println(xDif + " === " + yDif);
//        System.out.println("Available: " + available);

        int xDif = coords.x - tile.getCoords().x;
        int yDif = coords.y - tile.getCoords().y;

        //IF AT DISTANCE
        if ((xDif==1 || xDif==-1 || xDif==0) && (yDif==1 || yDif==-1 || yDif==0)){
            if (available) {

                //IF TILE CONTENT IS EMPTY, MOVE
                if (tile.getCont() == null) {
                    objective = tile;
                    available = false;
                    objective.setCont(this);
                    this.tile.setSelected(false);
                    this.tile.setCont(null);

                    unitAction = Action.MOVE;

                }else {

                    if (tile.getCont() instanceof Planet && ((Planet) tile.getCont()).getPlanetBase() == null|| tile.getCont() instanceof Moon && ((Moon) tile.getCont()).getMoonBase() == null) {
                        objective = tile;
                        unitAction = Action.BUILD;
                    }else

                    if (tile.getCont() instanceof Unit && ((Unit) tile.getCont()).getPlayer() != player || tile.getCont() instanceof Structure && ((Structure) tile.getCont()).getPlayer() != player || tile.getCont() instanceof Planet && ((Planet) tile.getCont()).getPlanetBase().getPlayer() != player|| tile.getCont() instanceof Moon && ((Moon) tile.getCont()).getMoonBase().getPlayer() != player) {
                        objective = tile;
                        unitAction = Action.ATTACK;
                    }
                }
            }

        }


    }

    public void move(Tile tile) {

        if ((int)coords.rawX == (int)objective.getCoords().rawX &&
                (int)coords.rawY == (int)objective.getCoords().rawY){

            available = true;
            finishMove(tile);
            coords = new Coords(coords.rawX, coords.rawY);
            unitAction = Action.IDLE;

        }else {

            if ((int)coords.rawX < (int)objective.getCoords().rawX)
                coords.rawX += speed;
            if ((int)coords.rawX > (int)objective.getCoords().rawX)
                coords.rawX += speed*-1;

            if ((int)coords.rawY < (int)objective.getCoords().rawY)
                coords.rawY += speed;
            if ((int)coords.rawY > (int)objective.getCoords().rawY)
                coords.rawY += speed*-1;
        }

    }

    private void attack(Tile tile) {

        GameEntity target = tile.getCont();
        if (tile.getCont() != null) {
            if (tile.getCont() != target) {
                unitAction = Action.IDLE;
            } else {
                target.damage(weapon);
            }
        }else unitAction = Action.IDLE;
    }



    private void finishMove(Tile tile) {

        //if (player.selectedUnit == this) tile.select
        this.tile = tile;
        this.tile.setCont(this);

    }



    public Player getPlayer() {
        return player;
    }



    @Override
    protected void death() {
        player.playerUnits.remove(this);
        tile.setCont(null);
    }

    public void addtoObjList(Tile tile){
        this.objectiveList.add(tile);
    }

    protected void setObjfromList(){

        setObjective(this.objectiveList.get(0));
        this.objectiveList.remove(0);

    }

    public void clearObjList() {
        this.objectiveList.clear();
    }


}
