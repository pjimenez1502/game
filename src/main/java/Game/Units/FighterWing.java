package Game.Units;

import Game.Map.GameMap;
import Game.Player.Player;
import javafx.scene.canvas.GraphicsContext;

public class FighterWing extends Unit {


    public FighterWing(int x, int y, GameMap map, GraphicsContext gc, Player player) {
        super(x, y, "FighterWing.png", map, gc, player);

        maxHealth = 1000;
        health = maxHealth;
        weapon = 4;
        speed = 1;

    }

    public static int[] getPrice(){
        return new int[]{250, 0};
    }

}
