package Game.Units;

import Game.Map.GameMap;
import Game.Map.Tile;
import Game.Player.Player;

import Game.Structures.Structure;
import Game.Terrain.Moon;
import Game.Terrain.Planet;
import javafx.scene.canvas.GraphicsContext;

public class Hauler extends Unit{


    public Hauler(int x, int y, GameMap map, GraphicsContext gc, Player player) {
        super(x, y, "worker.png", map, gc, player);

        maxHealth = 500;
        health = maxHealth;
        weapon = 1;
        speed = 1;
        //speed = 0.3;



    }

    @Override
    protected void build(Tile objective) {
        //System.out.println("BUILDING");


        //If objective is planet
        if (objective.getCont() instanceof Planet) {

            //if planetbase is null, build new planetbase
            if (((Planet) objective.getCont()).getPlanetBase() == null) {
                ((Planet) objective.getCont()).buildBase(player);
            }else {

                //if planetbase is in build progress, build
                if (((Planet) objective.getCont()).getPlanetBase().structureState == Structure.State.BUILDING){
                    ((Planet) objective.getCont()).getPlanetBase().build();
                }
            }

        }else if (objective.getCont() instanceof Moon) {
            //if planetbase is null, build new planetbase
            if (((Moon) objective.getCont()).getMoonBase() == null) {
                ((Moon) objective.getCont()).buildBase(player);
            }else {

                //if planetbase is in build progress, build
                if (((Moon) objective.getCont()).getMoonBase().structureState == Structure.State.BUILDING){
                    ((Moon) objective.getCont()).getMoonBase().build();
                }
            }
        }
    }

    public static int[] getPrice(){
        return new int[]{100, 0};
    }
}
