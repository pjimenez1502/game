package Game;

import Game.Map.GameMap;
import Game.Map.Tile;
import Game.Player.MachinePlayer;
import Game.Player.Player;
import Game.Structures.Structure;
import Game.Terrain.Moon;
import Game.Terrain.Planet;
import Game.Structures.Station;
import Game.Terrain.Terrain;
import Game.Units.*;
import javafx.animation.AnimationTimer;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.ArrayList;



public class Game {

        private ArrayList<Terrain> terrainElements = new ArrayList<>();



        public GameMap gamemap;

        private GraphicsContext gc;

        private Image objectiveImage;


        Game(GraphicsContext gc, Scene scene){
            this.gc = gc;


            gamemap = new GameMap(gc);

            Player player1 = new Player(Color.BLUE);

//            Player player2 = new Player(Color.RED);

            Player player2 = new MachinePlayer(gamemap);

            new Mouse(this, gamemap, player1, scene);





            //objectiveImage
            //objectiveImage = new Image("objective.png");



            //Elements at game start

            //TERRAIN
            terrainElements.add(new Planet(3,2,gamemap,gc));
            terrainElements.add(new Planet(12,9,gamemap,gc));
            terrainElements.add(new Planet(7,6,gamemap,gc));

            terrainElements.add(new Planet(8,0,gamemap,gc));
            terrainElements.add(new Planet(8,11,gamemap,gc));

            terrainElements.add(new Planet(13,2,gamemap,gc));
            terrainElements.add(new Planet(2,9,gamemap,gc));



            terrainElements.add(new Moon(3,3, gamemap, gc));
            terrainElements.add(new Moon(12,8, gamemap, gc));
            terrainElements.add(new Moon(7,5,gamemap,gc));
            terrainElements.add(new Moon(8,7,gamemap,gc));

            terrainElements.add(new Moon(14,1,gamemap,gc));
            terrainElements.add(new Moon(1,10,gamemap,gc));







            player1.playerUnits.add(new FighterWing(2,2, gamemap, gc, player1));
            player1.playerUnits.add(new Hauler(2,1, gamemap, gc, player1));
            player1.playerUnits.add(new AssaultShip(1,3,gamemap,gc,player1));




            player2.playerUnits.add(new FighterWing(13,9, gamemap, gc, player2));
            player2.playerUnits.add(new Hauler(15,11, gamemap, gc, player2));



            player2.playerUnits.add(new Destroyer(15,9,gamemap,gc,player2));
            player2.playerUnits.add(new Destroyer(14,11,gamemap,gc,player2));
            player2.playerUnits.add(new Destroyer(11,9,gamemap,gc,player2));

            player2.playerUnits.add(new AssaultShip(15,8,gamemap,gc,player2));
            player2.playerUnits.add(new AssaultShip(14,9,gamemap,gc,player2));
            player2.playerUnits.add(new AssaultShip(12,11,gamemap,gc,player2));
            player2.playerUnits.add(new AssaultShip(13,11,gamemap,gc,player2));
            player2.playerUnits.add(new AssaultShip(13,9,gamemap,gc,player2));

            player2.playerUnits.add(new FighterWing(13,8, gamemap, gc, player2));
            player2.playerUnits.add(new FighterWing(11,10, gamemap, gc, player2));
            player2.playerUnits.add(new FighterWing(12,10, gamemap, gc, player2));



            Station pl1st = new Station(1,1, gamemap, gc, player1);
            player1.playerStructures.add(pl1st);

            Station pl2st = new Station(14,10, gamemap, gc, player2);
            player2.playerStructures.add(pl2st);



            //GameLoop
            new AnimationTimer(){

                @Override
                public void handle(long l) {

                    gamemap.draw();

                    for (Terrain terrain: terrainElements) {
                        terrain.draw();
                    }



                    for (Unit unit : player1.playerUnits){
                        unit.update();
                        unit.draw();
                    }
                    for (Structure structure : player1.playerStructures){
                        structure.update();
                        structure.draw();
                    }



                    for (Unit unit : player2.playerUnits){
                        unit.update();
                        unit.draw();
                    }
                    for (Structure structure : player2.playerStructures){
                        structure.update();
                        structure.draw();
                    }

                    gc.setStroke(Color.WHITE);
                    gc.strokeText("Metal: " + player1.getResourceManager().getMetals() + "      Dust: " + player1.getResourceManager().getDust(), 1160, 885);


                    if (pl1st.health <= 0){

                        gc.setStroke(Color.WHITE);
                        gc.setFill(player2.getColor());
                        gc.setFont(new Font("Courier New", 120));
                        gc.strokeText("Player 2 Wins",400,400 );
                        gc.fillText("Player 2 Wins",400,400 );

                        stop();

                    }else if (pl2st.health <= 0){

                        gc.setStroke(Color.WHITE);
                        gc.setFill(player1.getColor());
                        gc.setFont(new Font("Courier New", 120));
                        gc.strokeText("Player 1 Wins",400,400 );
                        gc.fillText("Player 1 Wins",400,400 );

                        stop();

                    }


                }
            }.start();

        }








}
