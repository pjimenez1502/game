package Game;

import Game.Map.GameMap;
import Game.Map.Tile;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public abstract class GameEntity {

    protected Coords coords;
    protected Tile tile;
    protected Image image;
    protected GraphicsContext gc;
    protected GameMap map;
    protected int maxHealth;
    protected int health;

    public GameEntity(int x, int y, String imgstr, GameMap map, GraphicsContext gc){

        coords = new Coords(x,y);
        image = new Image(imgstr);
        tile = map.getMap()[x][y];
        this.gc = gc;
        this.map = map;
    }

    public void update(){

    }

    public void draw(){
        gc.drawImage(image, coords.rawX, coords.rawY);
    }



    public void damage(float weapon) {

        health -= weapon;

        if (health < 0){
            death();
        }
    }

    protected abstract void death();

    public Coords getCoords() {
        return coords;
    }
}
