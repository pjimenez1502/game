package Game.Structures;

import Game.Map.GameMap;
import Game.Map.Tile;
import Game.Player.Player;
import Game.Units.AssaultShip;
import Game.Units.Destroyer;
import Game.Units.FighterWing;
import Game.Units.Hauler;
import javafx.scene.canvas.GraphicsContext;

public class Station extends Structure {

    int[] shipProgress = new int[6];

    public Station(int x, int y, GameMap map, GraphicsContext gc, Player player) {
        super(x, y, "station.png", map, gc, player);

        tile.setCont(this);

        maxHealth = 10000;
        health = maxHealth;

    }

    @Override
    public void draw() {
        super.draw();
        if (player.getSelected() == this){
            if (map.getMap()[coords.x-1][coords.y-1].getCont() == null) {
                gc.strokeText("new Hauler", coords.rawX - 35, coords.rawY - 20);
            }
            if (map.getMap()[coords.x-1][coords.y].getCont() == null) {
                gc.strokeText("new Hauler", coords.rawX-80, coords.rawY+50);
            }
            if (map.getMap()[coords.x+1][coords.y].getCont() == null) {
                gc.strokeText("new Wing", coords.rawX+120, coords.rawY+50);
            }
            if (map.getMap()[coords.x][coords.y+1].getCont() == null) {
                gc.strokeText("new Wing", coords.rawX+70, coords.rawY+120);
            }
            if (map.getMap()[coords.x][coords.y-1].getCont() == null) {
                gc.strokeText("new Destroyer", coords.rawX+55, coords.rawY-20);
            }
            if (map.getMap()[coords.x-1][coords.y+1].getCont() == null) {
                gc.strokeText("new Assaultship", coords.rawX-35, coords.rawY+120);
            }
        }
    }

    public void createShip(Tile tile) {

        if (tile.getCoords().x == coords.x-1 && tile.getCoords().y == coords.y-1 && map.getMap()[coords.x-1][coords.y-1].getCont() == null){
            if (player.getResourceManager().getMetals() >= Hauler.getPrice()[0] && player.getResourceManager().getDust() >= Hauler.getPrice()[1]){
                player.playerUnits.add(new Hauler(coords.x-1, coords.y-1, map, gc, player));

                player.getResourceManager().addMetals(-Hauler.getPrice()[0]);
                player.getResourceManager().addDust(-Hauler.getPrice()[1]);
            }


        }else if (tile.getCoords().x == coords.x-1 && tile.getCoords().y == coords.y && map.getMap()[coords.x-1][coords.y].getCont() == null){
            if (player.getResourceManager().getMetals() >= Hauler.getPrice()[0] && player.getResourceManager().getDust() >= Hauler.getPrice()[1]){
                player.playerUnits.add(new Hauler(coords.x-1, coords.y, map, gc, player));

                player.getResourceManager().addMetals(-Hauler.getPrice()[0]);
                player.getResourceManager().addDust(-Hauler.getPrice()[1]);
            }


        }else if (tile.getCoords().x == coords.x+1 && tile.getCoords().y == coords.y && map.getMap()[coords.x+1][coords.y].getCont() == null) {
            if (player.getResourceManager().getMetals() >= FighterWing.getPrice()[0] && player.getResourceManager().getDust() >= FighterWing.getPrice()[1]){
                player.playerUnits.add(new FighterWing(coords.x+1, coords.y, map, gc, player));

                player.getResourceManager().addMetals(-FighterWing.getPrice()[0]);
                player.getResourceManager().addDust(-FighterWing.getPrice()[1]);
            }


        }else if (tile.getCoords().x == coords.x && tile.getCoords().y == coords.y+1 && map.getMap()[coords.x][coords.y+1].getCont() == null){
            if (player.getResourceManager().getMetals() >= FighterWing.getPrice()[0] && player.getResourceManager().getDust() >= FighterWing.getPrice()[1]){
                player.playerUnits.add(new FighterWing(coords.x, coords.y+1, map, gc, player));

                player.getResourceManager().addMetals(-FighterWing.getPrice()[0]);
                player.getResourceManager().addDust(-FighterWing.getPrice()[1]);
            }


        }else if (tile.getCoords().x == coords.x && tile.getCoords().y == coords.y-1 && map.getMap()[coords.x][coords.y-1].getCont() == null){
            if (player.getResourceManager().getMetals() >= Destroyer.getPrice()[0] && player.getResourceManager().getDust() >= Destroyer.getPrice()[1]){
                player.playerUnits.add(new Destroyer(coords.x, coords.y-1, map, gc, player));

                player.getResourceManager().addMetals(-Destroyer.getPrice()[0]);
                player.getResourceManager().addDust(-Destroyer.getPrice()[1]);
            }


        }else if (tile.getCoords().x == coords.x-1 && tile.getCoords().y == coords.y+1 && map.getMap()[coords.x-1][coords.y+1].getCont() == null){
            if (player.getResourceManager().getMetals() >= AssaultShip.getPrice()[0] && player.getResourceManager().getDust() >= AssaultShip.getPrice()[1]){
                player.playerUnits.add(new AssaultShip(coords.x-1, coords.y+1, map, gc, player));

                player.getResourceManager().addMetals(-AssaultShip.getPrice()[0]);
                player.getResourceManager().addDust(-AssaultShip.getPrice()[1]);
            }

        }

    }



}
