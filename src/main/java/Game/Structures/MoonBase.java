package Game.Structures;

import Game.Map.GameMap;
import Game.Player.Player;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class MoonBase extends Structure {

    public MoonBase(int x, int y, GameMap map, GraphicsContext gc, Player player) {

        super(x, y, "moonBase.png", map, gc, player);

        maxprogress = 1000;
        maxHealth = 10000;
        health = maxHealth;

        structureState = State.BUILDING;

    }

    @Override
    public void update() {

        switch (structureState){
            case BUILT:

                countdown ++;
                if (countdown >= 60){
                    countdown = 0;
                    player.getResourceManager().addDust(4);
                }

                break;
            case BUILDING:
                if (buildProgress >= maxprogress){
                    structureState = State.BUILT;
                }
                break;
        }

    }

    @Override
    public void draw() {
        super.draw();
    }

    public void build(){
        buildProgress++;
    }


}
