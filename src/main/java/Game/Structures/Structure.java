package Game.Structures;

import Game.GameEntity;
import Game.Map.GameMap;
import Game.Player.Player;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;


public class Structure extends GameEntity {

    protected int countdown;
    protected int buildProgress;
    protected int maxprogress;

    public enum State {
        BUILDING, BUILT
    }

    public State structureState;

    protected Player player;

    public Structure(int x, int y, String imgstr, GameMap map, GraphicsContext gc, Player player) {
        super(x, y, imgstr, map, gc);

        this.player = player;

        draw();

        player.playerStructures.add(this);
    }

    @Override
    public void draw() {
        super.draw();

        gc.setStroke(player.getColor());
        gc.strokeText(health + "/" + maxHealth, coords.rawX+20, coords.rawY+80);

        if (this.structureState == State.BUILDING){
            gc.setStroke(Color.WHITE);
            gc.strokeText( (int)((float)buildProgress/(float)maxprogress*100) + "%", coords.rawX + 40, coords.rawY + 40);
        }

    }

    public Player getPlayer() {
        return player;
    }

    @Override
    protected void death() {
        player.playerStructures.remove(this);
        tile.setCont(null);
    }
    
}
