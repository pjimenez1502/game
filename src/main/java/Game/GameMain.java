package Game;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class GameMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {

        stage.setTitle("gamescreen");

        Group root = new Group();
        Scene scene = new Scene(root);
        stage.setScene(scene);

        Canvas gamescreen = new Canvas(1600, 900);

        root.getChildren().add(gamescreen);

        GraphicsContext gc = gamescreen.getGraphicsContext2D();


        Game game = new Game(gc, scene);





        stage.setResizable(false);
        stage.show();

    }


}
