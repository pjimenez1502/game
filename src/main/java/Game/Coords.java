package Game;

public class Coords {

    public double rawX;
    public double rawY;
    public int x;
    public int y;

    public Coords(int x, int y){

        this.x = x;
        this.y = y;

        rawX = x*96;
        rawY = y*70;

        if (this.y % 2 == 0)
            rawX+=48;

    }

    public Coords (double rawX, double rawY){

        this.rawX = rawX;
        this.rawY = rawY;

        y = (int) rawY/70;

        if(y%2 == 0){
            rawX-=48;
        }

        x = (int) rawX/96;

    }

    public boolean equals(Coords obj) {
        if (this.x == obj.x && this.y == obj.y)
            return true;
        else return false;
    }


    @Override
    public String toString() {
        return "[X] = " + x + " [Y] = " + y;
        //return "[X] = " + rawX + " [Y] = " + rawY;
    }
}
