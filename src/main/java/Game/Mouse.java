package Game;

import Game.Map.GameMap;
import Game.Map.Tile;
import Game.Player.Player;
import Game.Structures.Station;
import Game.Structures.Structure;
import Game.Units.Unit;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;


public class Mouse {

    private Player player;
    private Game game;
    private GameMap map;

    boolean shift;

    public Mouse(Game game, GameMap map, Player player, Scene scene){
        this.game = game;
        this.map = map;
        this.player = player;

        scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getButton() == MouseButton.SECONDARY){
                    rightClick(e);

                }else if (e.getButton() == MouseButton.PRIMARY){
                    leftClick(e);
                }
            }
        });





        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {

                //ADD TO CONTROLGROUPS
                if (keyEvent.isControlDown() && (keyEvent.getText().equals("0") ||
                        keyEvent.getText().equals("1") ||
                        keyEvent.getText().equals("2") ||
                        keyEvent.getText().equals("3") ||
                        keyEvent.getText().equals("4") ||
                        keyEvent.getText().equals("5") ||
                        keyEvent.getText().equals("6") ||
                        keyEvent.getText().equals("7") ||
                        keyEvent.getText().equals("8") ||
                        keyEvent.getText().equals("9")))
                {
                    player.addtoCtrlGroup(Integer.parseInt(keyEvent.getText()));
                }

                //OPEN CONTROLGROUP
                if (keyEvent.getText().equals("0") ||
                        keyEvent.getText().equals("1") ||
                        keyEvent.getText().equals("2") ||
                        keyEvent.getText().equals("3") ||
                        keyEvent.getText().equals("4") ||
                        keyEvent.getText().equals("5") ||
                        keyEvent.getText().equals("6") ||
                        keyEvent.getText().equals("7") ||
                        keyEvent.getText().equals("8") ||
                        keyEvent.getText().equals("9"))
                {
                    player.openCtrlGroup(Integer.parseInt(keyEvent.getText()));
                }

                if (keyEvent.isShiftDown()){
                    setShift(true);
                } else {
                    setShift(false);
                }

            }
        });

    }

    public void setShift(boolean shift) {
        this.shift = shift;
    }

    public boolean isShiftDown(){
        return shift;
    }

    public void leftClick(MouseEvent mev){

        Coords c = new Coords(mev.getX()+20, mev.getY()+20);
        Tile tile = map.getMap()[c.x][c.y];

        if (tile.getCont() != null) {
            player.setSelected(null);
            map.selectTile(tile);

            if (tile.getCont() instanceof Unit) {

                if (((Unit) tile.getCont()).getPlayer() == player)
                    player.setSelected(tile.getCont());

            } else if (tile.getCont() instanceof Structure) {

                if (((Structure) tile.getCont()).getPlayer() == player)
                    player.setSelected(tile.getCont());

            } else {
                player.setSelected(null);
                map.selectTile(null);
            }
        }

    }

    public void rightClick(MouseEvent mev){

        Coords c = new Coords(mev.getX(), mev.getY());

        if (player.getSelected() instanceof Unit){
            if (isShiftDown()){
                ((Unit) player.getSelected()).addtoObjList(map.getMap()[c.x][c.y]);
            }else {
                ((Unit) player.getSelected()).setObjective(map.getMap()[c.x][c.y]);
                ((Unit) player.getSelected()).clearObjList();
            }
        }else if (player.getSelected() instanceof Station){
            ((Station) player.getSelected()).createShip(map.getMap()[c.x][c.y]);
        }

    }

}
