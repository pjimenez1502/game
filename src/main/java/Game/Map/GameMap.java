package Game.Map;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class GameMap {

    private GraphicsContext gc;
    private Tile[][] map = new Tile[16][12];

    private Image background = new Image("map.png");

    public GameMap(GraphicsContext gc){

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                map[i][j] = new Tile(i,j);
            }
        }

        this.gc = gc;

    }

    public void draw() {
        gc.drawImage(background, 0, 0);

        for (Tile[] row: map) {
            for (Tile tile: row) {
                gc.drawImage(tile.tileimg, tile.getCoords().rawX, tile.getCoords().rawY);
                if (tile.getSelected()){
                    gc.drawImage(tile.selectedimg, tile.getCoords().rawX, tile.getCoords().rawY);
                }
            }
        }
    }

    public Tile[][] getMap(){
        return map;
    }

    public void getMapValues(){

        for (Tile[] row: map) {
            for (Tile tile: row) {
                System.out.print(tile.getCont());
            }
            System.out.println();
        }
    }

    public void selectTile(Tile seltile){

        for (Tile[] row: map) {
            for (Tile tile: row){
                tile.setSelected(false);
            }
        }
        if (seltile!=null)
        seltile.setSelected(true);
    }

}
