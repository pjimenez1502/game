package Game.Map;

import Game.Coords;
import Game.GameEntity;
import javafx.scene.image.Image;

import java.util.concurrent.RecursiveTask;

public class Tile {

    private GameEntity cont;
    private boolean selected;

    private Coords coords;

    public Image tileimg = new Image("tile.png");
    public Image selectedimg = new Image("selected.png");


    public Tile(int x,int y){

        coords = new Coords(x,y);
        cont = null;

    }

    public Coords getCoords() {
        return coords;
    }

    public void setCont(GameEntity cont){
        this.cont = cont;
    }

    public GameEntity getCont() {
        return cont;
    }

    public void setSelected(boolean b){
        this.selected = b;
    }

    public boolean getSelected(){
        return selected;
    }

}
