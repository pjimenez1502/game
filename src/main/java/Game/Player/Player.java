package Game.Player;

import Game.GameEntity;
import Game.Structures.Structure;
import Game.Units.Unit;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.util.ArrayList;

public class Player {

    private ResourceManager resourceManager;
    public ArrayList<Unit> playerUnits = new ArrayList<>();
    public ArrayList<Structure> playerStructures = new ArrayList<>();
    public ArrayList<GameEntity> ctrlGroups = new ArrayList<>();

    GameEntity selected;

    private Color color;

    public Player(Color color){

        this.color = color;

        resourceManager = new ResourceManager();

        //ControlGroups
        for (int i = 0; i < 10; i++) {
            ctrlGroups.add(null);
        }

    }

    public void setSelected(GameEntity selected) {
        this.selected = selected;
    }

    public GameEntity getSelected() {
        return selected;
    }

    public ResourceManager getResourceManager() {
        return resourceManager;
    }

    public Paint getColor() {
        return color;
    }


    public void addtoCtrlGroup(int num){
        ctrlGroups.set(num, getSelected());
    }

    public void openCtrlGroup(int num) {
        setSelected(ctrlGroups.get(num));
    }

}
