package Game.Player;


public class ResourceManager {

    private int metals;
    private int dust;

    public ResourceManager(){
        metals = 0;
        dust = 0;
    }

    public void addMetals(int quant){
        metals += quant;
    }

    public int getMetals() {
        return metals;
    }

    public void addDust(int quant){
        dust += quant;
    }

    public int getDust() {
        return dust;
    }

    public void draw(){

    }
}
