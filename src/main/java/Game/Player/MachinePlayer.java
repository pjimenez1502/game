package Game.Player;

import Game.Map.GameMap;
import Game.Structures.Structure;
import Game.Terrain.Moon;
import Game.Terrain.Planet;
import Game.Terrain.Terrain;
import Game.Units.Hauler;
import Game.Units.Unit;
import javafx.animation.AnimationTimer;
import javafx.scene.paint.Color;

public class MachinePlayer extends Player{

    private Player player = this;

    private int count;
    private int maxCount;

    private GameMap map;

    public MachinePlayer(GameMap map){
        super(Color.RED);

        this.map = map;


        machinePlay();

    }

    private void machinePlay(){

        new AnimationTimer(){

            @Override
            public void handle(long l) {

                count++;

                if (count >= maxCount){
                    count = 0;
                    maxCount = ((int)(Math.random() * ((20 - 5)+1)) +5)*60 ;
                }

            if (count % 10 == 0) {

                for (Unit unit : playerUnits) {

                    if (unit instanceof Hauler){

                        try {

                            if (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y - 1].getCont() instanceof Terrain &&

                                    (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y - 1].getCont() instanceof Planet && ((Planet)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y - 1].getCont()).getPlanetBase() == null || ((Planet)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y - 1].getCont()).getPlanetBase().structureState == Structure.State.BUILDING) ||
                                    (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y - 1].getCont() instanceof Moon && ((Moon)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y - 1].getCont()).getMoonBase() != null || ((Moon)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y - 1].getCont()).getMoonBase().structureState == Structure.State.BUILDING)) {

                                unit.setObjective(map.getMap()[unit.getCoords().x - 1][unit.getCoords().y - 1]);

                            } else if (map.getMap()[unit.getCoords().x][unit.getCoords().y - 1].getCont() instanceof Terrain &&

                                    (map.getMap()[unit.getCoords().x][unit.getCoords().y - 1].getCont() instanceof Planet && ((Planet)map.getMap()[unit.getCoords().x][unit.getCoords().y - 1].getCont()).getPlanetBase() == null || ((Planet)map.getMap()[unit.getCoords().x][unit.getCoords().y - 1].getCont()).getPlanetBase().structureState == Structure.State.BUILDING) ||
                                    (map.getMap()[unit.getCoords().x][unit.getCoords().y - 1].getCont() instanceof Moon && ((Moon)map.getMap()[unit.getCoords().x][unit.getCoords().y - 1].getCont()).getMoonBase() != null || ((Moon)map.getMap()[unit.getCoords().x][unit.getCoords().y - 1].getCont()).getMoonBase().structureState == Structure.State.BUILDING)) {

                                unit.setObjective(map.getMap()[unit.getCoords().x][unit.getCoords().y - 1]);

                            } else if (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y].getCont() instanceof Terrain &&

                                    (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y].getCont() instanceof Planet && ((Planet)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y].getCont()).getPlanetBase() == null || ((Planet)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y].getCont()).getPlanetBase().structureState == Structure.State.BUILDING) ||
                                    (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y].getCont() instanceof Moon && ((Moon)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y].getCont()).getMoonBase() != null || ((Moon)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y].getCont()).getMoonBase().structureState == Structure.State.BUILDING)) {

                                unit.setObjective(map.getMap()[unit.getCoords().x - 1][unit.getCoords().y]);

                            } else if (map.getMap()[unit.getCoords().x + 1][unit.getCoords().y].getCont() instanceof Terrain &&

                                    (map.getMap()[unit.getCoords().x + 1][unit.getCoords().y].getCont() instanceof Planet && ((Planet)map.getMap()[unit.getCoords().x + 1][unit.getCoords().y].getCont()).getPlanetBase() == null || ((Planet)map.getMap()[unit.getCoords().x + 1][unit.getCoords().y].getCont()).getPlanetBase().structureState == Structure.State.BUILDING) ||
                                    (map.getMap()[unit.getCoords().x + 1][unit.getCoords().y].getCont() instanceof Moon && ((Moon)map.getMap()[unit.getCoords().x + 1][unit.getCoords().y].getCont()).getMoonBase() != null || ((Moon)map.getMap()[unit.getCoords().x + 1][unit.getCoords().y].getCont()).getMoonBase().structureState == Structure.State.BUILDING)) {

                                unit.setObjective(map.getMap()[unit.getCoords().x + 1][unit.getCoords().y]);

                            } else if (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y + 1].getCont() instanceof Terrain &&

                                    (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y + 1].getCont() instanceof Planet && ((Planet)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y + 1].getCont()).getPlanetBase() == null || ((Planet)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y + 1].getCont()).getPlanetBase().structureState == Structure.State.BUILDING) ||
                                    (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y + 1].getCont() instanceof Moon && ((Moon)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y + 1].getCont()).getMoonBase() != null || ((Moon)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y + 1].getCont()).getMoonBase().structureState == Structure.State.BUILDING)) {

                                unit.setObjective(map.getMap()[unit.getCoords().x - 1][unit.getCoords().y + 1]);

                            } else if (map.getMap()[unit.getCoords().x][unit.getCoords().y + 1].getCont() instanceof Terrain &&

                                    (map.getMap()[unit.getCoords().x][unit.getCoords().y + 1].getCont() instanceof Planet && ((Planet)map.getMap()[unit.getCoords().x][unit.getCoords().y + 1].getCont()).getPlanetBase() == null || ((Planet)map.getMap()[unit.getCoords().x][unit.getCoords().y + 1].getCont()).getPlanetBase().structureState == Structure.State.BUILDING) ||
                                    (map.getMap()[unit.getCoords().x][unit.getCoords().y + 1].getCont() instanceof Moon && ((Moon)map.getMap()[unit.getCoords().x][unit.getCoords().y + 1].getCont()).getMoonBase() != null || ((Moon)map.getMap()[unit.getCoords().x][unit.getCoords().y + 1].getCont()).getMoonBase().structureState == Structure.State.BUILDING)) {

                                unit.setObjective(map.getMap()[unit.getCoords().x][unit.getCoords().y + 1]);

                            } else {

                                unit.setObjective(map.getMap()[unit.getCoords().x + (int) (Math.random() * 3) - 1][unit.getCoords().y + (int) (Math.random() * 3) - 1]);

                            }
                        }catch (Exception e){
                            unit.setObjective(map.getMap()[unit.getCoords().x + (int) (Math.random() * 3) - 1][unit.getCoords().y + (int) (Math.random() * 3) - 1]);
                        }

                    }else {

                        try {

                            if (map.getMap()[unit.getCoords().x- 1][unit.getCoords().y - 1].getCont() instanceof Unit && ((Unit)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y - 1].getCont()).getPlayer() != player){

                                    unit.setObjective(map.getMap()[unit.getCoords().x - 1][unit.getCoords().y - 1]);

                            }else
                                if (map.getMap()[unit.getCoords().x][unit.getCoords().y - 1].getCont() instanceof Unit && ((Unit)map.getMap()[unit.getCoords().x][unit.getCoords().y - 1].getCont()).getPlayer() != player){

                                    unit.setObjective(map.getMap()[unit.getCoords().x][unit.getCoords().y - 1]);

                            }else
                                if (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y].getCont() instanceof Unit && ((Unit)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y].getCont()).getPlayer() != player){

                                    unit.setObjective(map.getMap()[unit.getCoords().x - 1][unit.getCoords().y]);

                            }else
                                if (map.getMap()[unit.getCoords().x + 1][unit.getCoords().y].getCont() instanceof Unit && ((Unit)map.getMap()[unit.getCoords().x + 1][unit.getCoords().y].getCont()).getPlayer() != player){

                                    unit.setObjective(map.getMap()[unit.getCoords().x + 1][unit.getCoords().y]);

                            }else
                                if (map.getMap()[unit.getCoords().x - 1][unit.getCoords().y +1].getCont() instanceof Unit && ((Unit)map.getMap()[unit.getCoords().x - 1][unit.getCoords().y + 1].getCont()).getPlayer() != player){

                                    unit.setObjective(map.getMap()[unit.getCoords().x - 1][unit.getCoords().y + 1]);

                            }else
                                if (map.getMap()[unit.getCoords().x][unit.getCoords().y + 1].getCont() instanceof Unit && ((Unit)map.getMap()[unit.getCoords().x][unit.getCoords().y + 1].getCont()).getPlayer() != player){

                                    unit.setObjective(map.getMap()[unit.getCoords().x][unit.getCoords().y + 1]);
                                }else {

                                unit.setObjective(map.getMap()[unit.getCoords().x + (int) (Math.random() * 3) - 1][unit.getCoords().y + (int) (Math.random() * 3) - 1]);

                            }
                        }catch (Exception e){

                            unit.setObjective(map.getMap()[unit.getCoords().x + (int) (Math.random() * 3) - 1][unit.getCoords().y + (int) (Math.random() * 3) - 1]);

                        }

                    }


                }

            }


            }
        }.start();


    }





}
